import React from 'react'
import * as FaIcons from "react-icons/fa"
import * as AiIcons from "react-icons/ai"

export const SidebarData = [
    {
      title: 'Home',
      path: 'home' ,
      cName: 'nav-text'
    },
    {
        title: 'About',
        path: 'about' ,
        cName: 'nav-text'
    },
    {
        title: 'Wine-Dish Pairing',
        path: 'winepair-section' ,
        cName: 'nav-text'
    },
    {
        title: 'Wine of the month',
        path: 'winemonth-section' ,
        cName: 'nav-text'
      }
]
