import React , {useState, useEffect} from 'react'
import { SliderData } from './SliderData'
import { FaArrowRight,FaArrowLeft } from 'react-icons/fa'
import Fade from 'react-reveal/Fade';
import { Link } from 'react-scroll';
import "./ImageSlider.css"
import "../App.css"

export const ImageSlider = ({slides}) => {
    const [current, setCurrent] = useState(0)
    const length = slides.length 
    
    const nextSlide = () => {
        setCurrent(current === length - 1 ? 0 : current + 1)
    }

    const prevSlide = () => {
        setCurrent(current === 0 ? length - 1  : current - 1)   
    }

    React.useEffect(() => {
        const intervalId = setInterval(() => {
          setCurrent((prev) => {
            return prev + 1 === slides.length ? 0 : prev + 1;
          });
        }, 5000);
        return () => {
          clearInterval(intervalId);
        };
      }, []);

    if(!Array.isArray(slides) || slides.length <= 0){
        return null
    }
    return (
        <Fade clear>
            <div>
                <section className='slider'>
                    <FaArrowLeft className = "left-arrow" onClick={prevSlide} />
                    <FaArrowRight className = "right-arrow" onClick={nextSlide} />
                    

                    {/* Image Loop */}
                    
                    {SliderData.map((slide, index)=>{
                        return(
                        <div className = {index === current ? 'slide active' : 'slide'} key = {index}>
                            {index === current && (<img src={slide.image} alt = "wine image" className="image"/>)}
                        </div>
                        )
                    })}
        
                </section>
                

                    {/* Title Loop */}    
                <section className='slider-title'>
                
                {SliderData.map((slide, index)=>{
                    return(
                    <div className = {index === current ? 'slide active' : 'slide'} key = {index}>
                    {index === current && (<div className = 'heading'>{slide.title}</div>)}
                    </div>
                    )
                })}

                </section>

                {/* Description Loop */}    
                <section className='slider-description'>
                
                {SliderData.map((slide, index)=>{
                    return(
                    <div className = {index === current ? 'slide active' : 'slide'} key = {index}>
                    {index === current && (<div className = 'description'>{slide.description}</div>)}
                    </div>
                    )
                })}

                </section>

                {/* Button Loop */}    
                <section className='slider-button'>
                
                {SliderData.map((slide, index)=>{
                    return(
                    <div className = {index === current ? 'slide active' : 'slide'} key = {index}>
                    {index === current && (<Link activeClass="active" to={slide.section} spy={true} smooth={true} duration={1000} class="button">{slide.button}</Link>)}
                    </div>
                    )
                })}

                </section>
        </div>
    </Fade>
    )
    console.log(current);
    console.log(setCurrent);
}

export default ImageSlider
