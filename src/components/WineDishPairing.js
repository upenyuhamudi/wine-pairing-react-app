import React from 'react'
import WinePairing from './WinePairing';
import DishPairing from './DishPairing';
import "./WineDishPairing.css";
import { Link } from 'react-scroll';
import Fade from 'react-reveal/Fade';



function WineDishPairing() {
    return (
        <section id="winepair-section">
            <Fade>
            <p className = 'section-heading'>Wine-Dish Pairing</p>
            </Fade>
            <div className = 'wine-app-container'>
                <div className= 'wine-pair'>
                    <Fade left> 
                    <WinePairing/>
                    </Fade>
                </div>
                <div className = 'dish-pair'>
                    <Fade right>
                    <DishPairing/>
                    </Fade>
                </div>
            </div>
            <Link activeClass="active" to="winemonth-section" spy={true} smooth={true} duration={1000} className="scroll-button">CHECK OUT SOME WINE RECOMMENDATIONS</Link>
        </section>
    )
}

export default WineDishPairing
