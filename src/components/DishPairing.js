import React, {useState} from 'react'

const apiKey = 'd4e96420aa6b41829d9408c06fc326e4';
const url = 'https://api.spoonacular.com/food/wine/pairing?food'

export const DishPairing = () => {
const [query, setQuery] = useState('');
const [dishpairing, setDishPairing] = useState({});

const search = evt =>{
    if (evt.key === "Enter"){
        fetch(`${url}=${query}&apiKey=${apiKey}`)
        .then(response =>response.json())
        .then(result => {
            setQuery('');
            setDishPairing(result);
            console.log(result);
        });
            
    }
}
    return (
            <div className ="card">
                <div className = "dish-pair-content">
                <h2 className = "subheading">Dish Pairing</h2>
                <input 
                    type = "text"
                    className = "search-bar"
                    placeholder = "Enter your food of choice...."
                    onChange = {e => setQuery(e.target.value )}
                    value={query}
                    onKeyPress={search}
                    />
                <p className = "wine-dish-description">{dishpairing.pairingText}</p>

                {(typeof dishpairing.message != "failure")?(<p className = "error-description">{dishpairing.message}</p>):("")}
                
                </div>            
            </div>
        
        
   
    )
}

export default DishPairing;
