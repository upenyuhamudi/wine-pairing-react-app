import React, {useState} from 'react';
import Jump from 'react-reveal/Jump';


const apiKey = 'd4e96420aa6b41829d9408c06fc326e4';
const url = 'https://api.spoonacular.com/food/wine/dishes?wine'

export const WinePairing = () => {
const [query, setQuery] = useState('');
const [winepairing, setWinePairing] = useState({});

const search = evt =>{
    if (evt.key === "Enter"){
        fetch(`${url}=${query}&apiKey=${apiKey}`)
        .then(response =>response.json())
        .then(result => {
            setQuery('');
            setWinePairing(result);
            console.log(result);
        });
            
    }

    
}
    return (
        <>
            <div className ="card">
                <div className = "wine-pair-content">
                    <h2 className = "subheading">Wine Pairing</h2>
                <input 
                        type = "text"
                        className = "search-bar"
                        placeholder = "Enter wine of choice...."
                        onChange = {e => setQuery(e.target.value )}
                        value={query}
                        onKeyPress={search}
                        /> 
                <p className = "wine-dish-description">{winepairing.text}</p>
                
                {/* Error Message */}
                    <div className = "error-message">
                        {(typeof winepairing.message != "failure")?(<p className = "error-description">{winepairing.message}</p>):('')}
                    </div>
                </div>
            </div>
        </>
   
    )
}

export default WinePairing;
