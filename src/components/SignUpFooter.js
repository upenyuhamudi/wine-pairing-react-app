import React from 'react'
import "./SignUpFooter.css"
import Fade from 'react-reveal/Fade';
import { Link } from 'react-scroll';
import * as FaIcons from "react-icons/fa"

export const SignUpFooter = () => {
    return (
        <div>
            <Fade>
                <p className = "signup-heading">Want wine recommendations every month?</p>
                <p className = "signup-subheading">Sign up for our newsletter</p>
                <div className = "signup-box">
                    <input 
                        type = "text"
                        className = "signup-bar"
                        placeholder = "Name"
                    />
                    <input 
                        type = "text"
                        className = "signup-bar"
                        placeholder = "E-mail Address"
                    />
                </div>
                
                <div className = "end">
                    <footer className = "footer-container">
                        <Link to='home' spy={true} smooth={true} duration={1000} className="footer"><p>Home</p></Link>
                        <Link to="about" spy={true} smooth={true} duration={1000} className="footer">About</Link>
                        <Link to="winepair-section" spy={true} smooth={true} duration={1000} className="footer">Wine-Dish Pairing</Link>
                        <Link to="winemonth-section" spy={true} smooth={true} duration={1000} className="footer">Wine of the month</Link>
                    </footer>
                    <div className = "social-media">
                    <a href="https://www.instagram.com/kumushawines/" target = "_blank" className = "socialmedia-icon"><FaIcons.FaInstagram/></a>
                    <a href="https://twitter.com/Winemag" target = "_blank" className = "socialmedia-icon"><FaIcons.FaTwitter/></a>
                    </div>
                    <p className = "company-name">vi(KNOW) © 2020</p>
                </div>
            </Fade>
        </div>
        
    )
}
