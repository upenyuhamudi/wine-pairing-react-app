export const WineMonthData = [
    {
        image: "https://www.ocado.com/productImages/637/63725011_0_640x640.jpg?identifier=d6a31177dcc67039cfb2c15ee4a85e13",
        wineName: 'Douglas Green Vineyard Cabernet Sauvignon 2017',
        wineDescription: 'A succulent medium bodied entry with delicious winterberry flavours supported by well defined ripe tannins for a softly textured juicy finish that lingers.',
        winePrice: 'R 73.00',
        wineLink: "https://www.news24.com/fin24/economy/alcohol-ban-reinstated-as-sa-moves-to-level-3-lockdown-20201228"
    },
    {
        image: "https://shop.soulliquor.com/images/thumbnails/1800/1800/detailed/1/Nederburg_Merlot.jpg",
        wineName: 'Nederburg Paarl Wines Merlot 2016',
        wineDescription: 'A well-rounded wine with attractive up-front plum and red fruit flavours and elegant tannins, with integrated oak nuances that round off the palate.',
        winePrice: 'R 89.00',
        wineLink: "https://www.news24.com/fin24/economy/alcohol-ban-reinstated-as-sa-moves-to-level-3-lockdown-20201228"
    },
    {
        image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPEcAfiriBrT3u1XdjC31QZ4Yoc19DZpwKyQ&usqp=CAU",
        wineName: 'Bonnievale Limited Release Chardonnay 2019',
        wineDescription: 'The Limited Release Chardonnay 2019 displays a lot of citrus, especially lime and grapefruit flavours that carry through to the pallet. On the pallet.',
        winePrice: 'R 112.00',
        wineLink: "https://www.news24.com/fin24/economy/alcohol-ban-reinstated-as-sa-moves-to-level-3-lockdown-20201228"
    }
    
]