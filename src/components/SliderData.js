export const SliderData = [
    {
        image: 'https://images.unsplash.com/photo-1506377247377-2a5b3b417ebb?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
        title: 'vi(KNOW)',
        description: 'Your Personal Sommelier',
        button: 'LEARN MORE',
        section: 'about' 
    },
    {
        image: 'https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
        title: 'Wine-Dish Pairing',
        description: 'Want to pair your meals with the right wine?',
        button: 'USE OUR ONLINE TOOL',
        section: 'winepair-section'
    },
    {
        image: 'https://images.unsplash.com/photo-1513618827672-0d7c5ad591b1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80',
        title: 'Wine of the week',
        description: 'Overwhelmed by the wide range of wines?',
        button: 'LET OUR SOMMS HELP YOU',
        section: 'winemonth-section'
    },
]