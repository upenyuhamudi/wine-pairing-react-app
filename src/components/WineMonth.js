import React from 'react';
import "./WineMonth.css";
import Fade from 'react-reveal/Fade';
import { WineMonthData } from './WineMonthData';


export const WineMonth = () => {
    return (
        
        <section id = "winemonth-section">          
            
            <Fade><p className = 'section-heading'>Wine of the month</p></Fade>
            <Fade bottom>
            <div className = "winemonth-container">
                {WineMonthData.map((wine, index)=>{
                    return(
                        
                        <div className ="wine-card">                         
                            <img className = "wine-image" src={wine.image}/>
                            <h2 className = "wine-heading">{wine.wineName}</h2>
                            <p className = "wine-description">{wine.wineDescription}</p>
                            <p className = "wine-price">{wine.winePrice}</p>
                            <a href={wine.wineLink} target ="_blank" className="buy-button">BUY NOW</a>
                        </div>
                    )
                })}
            </div>
            </Fade>
        </section>
        
        )
}
