import React from 'react'
import Navbar from './components/Navbar'
import ImageSlider from './components/ImageSlider';
import { SliderData } from './components/SliderData';
import About from './components/About';
import WineDishPairing from './components/WineDishPairing';
import { WineMonth } from './components/WineMonth';
import { SignUpFooter } from './components/SignUpFooter';
import './App.css'
<meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>



function App() {
  return (
    <>
    <div>
      <Navbar />
      <ImageSlider slides = {SliderData}/>;
      <About/>
      <WineDishPairing/>
      <WineMonth/>
      <SignUpFooter/>
    </div>
    </>
  );
}

export default App;
