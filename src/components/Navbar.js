import React, {useState} from 'react'
import * as FaIcons from "react-icons/fa"
import * as AiIcons from "react-icons/ai"
import { Link } from 'react-scroll';
import Fade from 'react-reveal/Fade';
import {SidebarData} from './SidebarData'
import "./Navbar.css"
import "../App.css"
import {IconContext} from 'react-icons'

function Navbar() {
    const [sidebar, setSidebar] = useState(false)
    
    const showSidebar = () => setSidebar(!sidebar)

    return (
        <section className = 'home'> 
        <Fade clear>
          <div className="navbar">
            <div className='menu-bars'>
                <FaIcons.FaBars color = 'white' onClick = {showSidebar}/>
            </div>
          </div> 
          <nav className = {sidebar ? 'nav-menu active' : 'nav-menu'}>
            <ul className='nav-menu-items' onClick = {showSidebar}>
               <li className = "navbar-toggle">
                  <AiIcons.AiOutlineClose color='white' className='menu-bars' onClick = {showSidebar}/>
               </li> 
               {SidebarData.map((item, index) => {
                   return(
                       <li key={index} className={item.cName}>
                           <Link activeClass="active" to={item.path} spy={true} smooth={true} duration={1000}>
                             <span onClick = {showSidebar}>{item.title}</span>
                           </Link>
                       </li>
                   )
               })}    
            </ul> 
          </nav> 
          </Fade>
        </section>
    )
}

export default Navbar
