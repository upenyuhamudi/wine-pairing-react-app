import React from 'react'
import Fade from 'react-reveal/Fade';
import { Link } from 'react-scroll';
import "./About.css"


const About = () => {
    return (
        <section id="about">
        <div className = 'about-section'>
            <div className = 'about-section-left'>
                <Fade left>
                <p className='about-heading'>vi(KNOW) aims to simplify the beautiful complexity of wine</p>
                <p className = 'about-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras viverra ex bibendum risus facilisis rhoncus. Cras ornare consectetur ex quis imperdiet.</p>
                <Link activeClass="active" to="winepair-section" spy={true} smooth={true} duration={1000} className="about-button">SCROLL DOWN</Link>
                </Fade>
            </div>
            <div className = 'about-section-right'>
                <Fade right>
                <img className = 'about-image' src='https://images.unsplash.com/photo-1561461056-77634126673a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80' className='about-image'/>
                </Fade>
            </div>
        </div>
        </section>
    )
}

export default About
